<?php

/**
 * Displays markup for the landing page.
 */

namespace lina\hw5\views;

class LandingView extends View
{
    /**
     * Renders markup to the landing page.
     * @param array $data data from the LandingController to be rendered
     */
    public function render($data)
    {
        ?>
        <!DOCTYPE html>
        <html>
            <head>
                <title></title>
                <meta charset="utf-8" />
                <script type="text/javascript">
                    var fountainWish = new Object();

                    /**
                     * Updates the fountain image to reflect fountain customizations.
                     */
                    function updateFountain()
                    {
                        var wish = document.getElementById("wish").value;
                        var yourName = document.getElementById("your_name").value;
                        var yourEmail = document.getElementById("your_email").value;
                        
                        var fountainImage = document.getElementById("fountain");

                        var fountainSelectColor = document.getElementById("fountainColor");
                        var fountainColorValue = fountainSelectColor.options[fountainSelectColor.selectedIndex].value;
                        fountainImage.src = "src/resources/fountain_" + fountainColorValue;

                        var fountainCheckCat = document.getElementById("includeCat");
                        var fountainCat = false;
                        if (fountainCheckCat.checked)
                        {
                            fountainCat = true;
                            fountainImage.src += "_cat";
                        }

                        var fountainCheckSmiley = document.getElementById("includeSmileyFace");
                        var fountainSmiley = false;
                        if (fountainCheckSmiley.checked)
                        {
                            fountainSmiley = true;
                            fountainImage.src += "_smiley";
                        }

                        fountainImage.src += ".png";
                        
                        //var recipients = {};
                        var recipientName = document.getElementById("recipientName").value;
                        var recipientEmail = document.getElementById("recipientEmail").value;

                        fountainWish.wish = wish;
                        fountainWish.yourName = yourName;
                        fountainWish.yourEmail = yourEmail;
                        fountainWish.src = fountainImage.src;
                        fountainWish.recipientName = recipientName;
                        fountainWish.recipientEmail = recipientEmail;
                        console.log(fountainWish);
                        
                        document.getElementById("fountain_src").value = fountainWish.src;
                    }
                </script>
                <script src="https://js.stripe.com/v2/"></script>
                <script>Stripe.setPublishableKey('<?=STRIPE_PUBLISHABLE_KEY ?>');</script>
            </head>
            <body>
                <div><a href="#">Español</a></div>
                <img src="src/resources/fountain_white.png" alt="Fountain" id="fountain" width="256" height="256">
                <form method="post" id="throw_coin_form">
                    <input type="hidden" id="fountain_src" name="fountain_src" value="" />
                    <input type="hidden" id="credit-token"  name="credit_token" value="" />
                    <h3>Wish</h3>
                    <p>
                        <label for="wish">Your wish</label>
                        <input type="text" id="wish" name="wish" value="" />
                    </p>
                    <p>
                        <label for="your_name">Your name</label>
                        <input type="text" id="your_name" name="your_name" value="" />
                    </p>
                    <p>
                        <label for="your_email">Your email</label>
                        <input type="text" id="your_email" name="your_email" value="" />
                    </p>
                    <hr>
                    <h3>Customize Fountain</h3>
                    <label for="fountainColor">Color</label>
                    <select id="fountainColor" name="fountainColor" onchange="updateFountain();">
                        <option value="white">White</option>
                        <option value="red">Red</option>
                        <option value="blue">Blue</option>
                    </select>
                    <p>
                        <label for="includeCat">Include a cat</label>
                        <input type="checkbox" id="includeCat" name="includeCat" value="cat" onchange="updateFountain();" />
                    </p>
                    <p>
                        <label for="includeSmileyFace">Include a smiley face</label>
                        <input type="checkbox" id="includeSmileyFace" name="includeSmileyFace" value="smiley" onchange="updateFountain();" />
                    </p>
                    <hr>
                    <h3>Recipients</h3>
                    <p>
                        <label for="recipientName">Recipient name:</label>
                        <input type="text" id="recipientName" name="recipientName" value="" />
                    </p>
                    <p>
                        <label for="recipientEmail">Recipient email:</label>
                        <input type="text" id="recipientEmail" name="recipientEmail" value="" />
                    </p>
                    <!--<button type="button">Add Recipient</button>-->
                    <hr>
                    <h3>Payment Information</h3>
                    <p>
                        <label for="card-number">Card Number:</label>
                        <input type="text" id="card-number" size="20" data-stripe='number' name="card-number" />
                    </p>
                    <p>
                        <label for="cvc">CVC:</label>
                        <input type="text" id="cvc" size="4" data-stripe='cvc' name="cvc" />
                    </p>
                    <p>
                        <label for="exp-month">Expiration Month:</label>
                        <input type="text" id="exp-month" size="2" data-stripe='exp-month' name="exp-month" />
                    </p>
                    <p>
                        <label for="exp-year">Expiration Year:</label>
                        <input type="text" id="exp-year" size="2" data-stripe='exp-year' name="exp-year" />
                    </p>
                    <p>
                        <input type="submit" id="throw_coin" name="throw_coin" value="Throw-A-Coin" />
                    </p>
                </form>
                <script>             
                    document.getElementById("throw_coin").onclick =
                    function(event)
                    {
                        updateFountain();
                        
                        var throw_coin_form = document.getElementById("throw_coin_form");
                        document.getElementById("throw_coin").disabled = true; // prevent additional clicks
                        Stripe.card.createToken(throw_coin_form, tokenResponseHandler);
                        event.preventDefault(); //prevent form submitting till get all clear
                    }
                        
                    function tokenResponseHandler(status, response) 
                    {
                        var throw_coin_form = document.getElementById("throw_coin_form");
                        if (response.error)
                        {
                            alert(response.error.message);
                            document.getElementById("throw_coin").disabled = false;
                        }
                        else
                        {
                            document.getElementById("credit-token").value = response.id;
                            throw_coin_form.submit();
                        }
                    }
                </script>
            </body>
        </html>
        <?php
    }
}