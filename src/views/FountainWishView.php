<?php

namespace lina\hw5\views;

require_once(dirname(__FILE__) . "/../../vendor/setasign/fpdf/fpdf.php");

/**
 * Displays a fountain wish as a PDF.
 */
class FountainWishView extends View
{
    /**
     * Renders a fountain wish based on input data.
     * @param $data array - contains the fountain wish result set 
     * that was retrieved in the controller.
     */
    public function render($data)
    {
        $pdf = new \FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Times','',12);
        $pdf->Image($data['wish']['fountain_src'], 10, 6, 30);
        $pdf->Cell(80);
        $pdf->Cell(40,10,'Fountain Wish',1,0,'C');
        $pdf->Ln(20);
        $pdf->Cell(0,20, $data['wish']['wish']);
        $pdf->Ln(20);
        $pdf->Cell(0,20, "From: " . $data['wish']['wisher_name']);
        $pdf->Output();
    }
}