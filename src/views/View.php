<?php

/**
 * Renders markup to pages.
 */

namespace lina\hw5\views;

abstract class View
{
    /**
     * Renders markup.
     * @param array $data data passed from the controller to be rendered
     */
    public abstract function render($data);
}
