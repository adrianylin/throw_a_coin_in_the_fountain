<?php

namespace lina\hw5\models;

/**
 * Handles the fountain_wish table and allows controllers to
 * interact with the fountain_wish table.
 */
class FountainWishModel extends Model
{
    /**
     * Variables to hold prepared statements.
     */
    private $getWishStmt;
    private $insertWishStmt;

    /**
     * Connects to the database and initializes prepared statements.
     */
    public function __construct()
    {
        $this->connectDb();
        $this->getWishStmt = $this->con->prepare('SELECT wish, wisher_name, wisher_email, fountain_src, recipient_name, recipient_email FROM fountain_wish WHERE id=?');
        $this->insertWishStmt = $this->con->prepare('INSERT INTO fountain_wish (wish, wisher_name, wisher_email, fountain_src, recipient_name, recipient_email) VALUES (?, ?, ?, ?, ?, ?)');
    }

    /**
     * Gets a fountain wish based on the input id.
     * @param $id int - fountain wish id to get
     * @return the result set of the fountain wish that is retrieved 
     */
    public function getWish($id)
    {
        $this->getWishStmt->bind_param('s', $id);
        $this->getWishStmt->execute();
        $result = mysqli_fetch_array($this->getWishStmt->get_result());
        return $result;
    }
    
    /**
     * Inserts a fountain wish.
     */
    public function insertWish($wish, $wisher_name, $wisher_email, $fountain_src, $recipient_name, $recipient_email)
    {
        $this->insertWishStmt->bind_param('ssssss', $wish, $wisher_name, $wisher_email, $fountain_src, $recipient_name, $recipient_email);
        $this->insertWishStmt->execute();
    }
}