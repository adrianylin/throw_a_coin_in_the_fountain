<?php

/**
 * Handles the database.
 */

namespace lina\hw5\models;

class Model
{
	public $con = null;

	/**
	 * Creates a database connection based on values in src/configs/Config.php.
	 */
	public final function connectDb()
	{
		$this->con = new \mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, DB_PORT, DB_SOCKET);
	}

	/**
	 * Closes a database connection.
	 */
	public final function closeDb()
	{
		$this->con->close();
	}
}
