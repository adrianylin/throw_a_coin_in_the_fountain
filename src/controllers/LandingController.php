<?php

namespace lina\hw5\controllers;

/**
 * Handles fountain wish submissions. 
 */
class LandingController extends Controller
{
    /**
     * Displays the fountain wish creation
     * form if this page is requested with a GET request. Displays confirmation
     * that the fountain wish was received if this page is requested with a
     * POST request. Handles PDF and email creation. Makes sure that users are 
     * charged for each fountain wish through Stripe.
     */
    public function processRequest()
    {
        $data = [];

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $message = "";
            $amount = WISH_CHARGE_AMOUNT;
            $token = $_POST['credit_token'];
            $success = $this->charge($amount, $token, $message);
            unset($_POST['credit_token']);
            if ($success)
            {
                $fountainWishModel = new \lina\hw5\models\FountainWishModel();
                $fountainWishModel->insertWish($_POST['wish'], $_POST['your_name'], $_POST['your_email'], $_POST['fountain_src'], $_POST['recipientName'], $_POST['recipientEmail']);
                $id = $fountainWishModel->con->insert_id;
                $fountainWishModel->closeDb();
                
                // send mail to the wisher
                mail($_POST['your_email'],
                      "Fountain Wish",
                      "Your fountain wish: " . BASE_URL . "/?c=FountainWishController&m=processRequest&id=" . $id,
                      "From: " . FOUNTAIN_WISH_EMAIL);
                // send mail to the recipient
                mail($_POST['recipientEmail'],
                      "Fountain Wish",
                      "You received a fountain wish: " . BASE_URL . "/?c=FountainWishController&m=processRequest&id=" . $id,
                      "From: " . FOUNTAIN_WISH_EMAIL);
                
                echo "<p>A link to your fountain wish has been sent to your email and your recipient's email.</p><p>A link to your fountain wish: " . BASE_URL . "/?c=FountainWishController&m=processRequest&id=" . $id . "</p>";
            }
            else
            {
                echo "$amount charge did not go through!";
                if (!empty($message))
                {
                    echo "<br />$message<br/>";
                }
            }
        }

        $landingView = new \lina\hw5\views\LandingView();
        $landingView->render($data);
    }
    
    /**
     * Charges a user the amount specified in STRIPE_CHARGE_CURRENCY.
     * @param $amount int - the amount to be charged in usd cents
     * @param $token string - the payment token for stripe charges
     * @param $message string - message shown to the user in stripe charges
     * @return true if the stripe charge succeeds
     * Note: the majority of code used in this function is from the
     * Stripe example presented in lecture slides.
     */
    function charge($amount, $token, &$message)
    {
        $charge = [
            "amount" => $amount,
            "currency" => STRIPE_CHARGE_CURRENCY,
            "source" => $token,
            "description" => STRIPE_CHARGE_DESCRIPTION
        ];
        $response = $this->getPage(STRIPE_CHARGE_URL, http_build_query($charge),
            STRIPE_SECRET_KEY . ":");
        $credit_info = json_decode($response, true);
        if (!empty($credit_info['message']))
        {
            $message = $credit_info['message'];
        }
        return isset($credit_info['status']) &&
            $credit_info['status'] == 'succeeded';
    }

    /**
     * CURL requests in order to process the Stripe charge.
     * @param $site - the url to process Stripe charges
     * @param $post_data - post data with charge information
     * @param $user_password - stripe secret key
     * @return true if the curl requests execute without error
     * Note: the majority of code used in this function is from the
     * Stripe example presented in lecture slides.
     */
    function getPage($site, $post_data = null, $user_password = null)
    {
        $agent = curl_init();
        curl_setopt($agent, CURLOPT_USERAGENT, STRIPE_CHARGE_USERAGENT);
        curl_setopt($agent, CURLOPT_URL, $site);
        curl_setopt($agent, CURLOPT_AUTOREFERER, true);
        curl_setopt($agent, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($agent, CURLOPT_NOSIGNAL, true);
        curl_setopt($agent, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($agent, CURLOPT_FAILONERROR, true);
        curl_setopt($agent, CURLOPT_TIMEOUT, TIMEOUT);
        curl_setopt($agent, CURLOPT_CONNECTTIMEOUT, TIMEOUT);
        //make lighttpd happier
        curl_setopt($agent, CURLOPT_HTTPHEADER, ['Expect:']);
        if ($post_data != null)
        {
            curl_setopt($agent, CURLOPT_POST, true);
            curl_setopt($agent, CURLOPT_POSTFIELDS, $post_data);
        }
        else
        {
            curl_setopt($agent, CURLOPT_HTTPGET, true);
        }
        if ($user_password != null)
        {
            curl_setopt($agent, CURLOPT_FAILONERROR, false);
            curl_setopt($agent, CURLOPT_USERPWD, $user_password);
            curl_setopt($agent, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($agent, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        }
        $response = curl_exec($agent);
        curl_close($agent);
        return $response;
    }
}
