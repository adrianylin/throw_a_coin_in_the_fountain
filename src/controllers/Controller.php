<?php

namespace lina\hw5\controllers;

/**
 * Base controller class. A controller receives requests from index.php,
 * does some calculation, chooses a view to render, and renders it. 
 */
abstract class Controller
{
    /**
     * Processes a web request.
     */
    public abstract function processRequest();
}
