<?php

namespace lina\hw5\controllers;

/**
 * Processes requests in getting a fountain wish.
 */
class FountainWishController extends Controller
{
    /**
     * Gets the fountain wish according to request parameters from the 
     * model and passes that information along to the view.
     */
    public function processRequest()
    {
        $data = [];
        
        $fountainWishModel = new \lina\hw5\models\FountainWishModel();
        $wish  = $fountainWishModel->getWish($_REQUEST['id']);
        $fountainWishModel->closeDb();

        $data['wish'] = $wish;
        
        $fountainWishView = new \lina\hw5\views\FountainWishView();
        $fountainWishView->render($data);
    }
}
