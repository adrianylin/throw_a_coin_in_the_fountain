<?php

require_once(dirname(__FILE__) . '/Config.php');

/**
 * Command line utility that initializes the fountain_wish database.
 * The fountain_wish database contains a fountain_wish table which
 * contains details such as the image, wish, and recipient of each
 * fountain wish.
 */

$con = new \mysqli(DB_HOST, DB_USER, DB_PASSWORD, '', DB_PORT, DB_SOCKET);
$query = 'CREATE DATABASE ' . DB_NAME;
$con->query($query);
if ($con->connect_error)
{
	print('Error creating database');
	exit;
}
else
{
    $con->select_db(DB_NAME);
    $file = fopen("CreateDB.sql", "r");
    if ($file)
    {
        while ($line = fgets($file))
        {
            $con->query($line);
        }
        fclose($file);
        print('Database created');
    }
    else
    {
        print('CreateDB.sql not found');
    }
    $con->close();
}