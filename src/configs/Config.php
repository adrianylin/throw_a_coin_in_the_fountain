<?php

/**
 * Configuration file.
 */

/**
 * Database
 */
define('DB_HOST', 'localhost');
define('DB_PORT', 3306);
define('DB_SOCKET', '');
define('DB_NAME', 'fountain_wish');
define('DB_USER', '');
define('DB_PASSWORD', '');

/**
 * Stripe
 */
define("STRIPE_SECRET_KEY", "");
define("STRIPE_PUBLISHABLE_KEY", "");
define("STRIPE_CHARGE_URL", "https://api.stripe.com/v1/charges");
define("STRIPE_CHARGE_CURRENCY", "usd");
define("STRIPE_CHARGE_DESCRIPTION", "FountainWish");
define("STRIPE_CHARGE_USERAGENT", "FountainWish");
define("WISH_CHARGE_AMOUNT", 100);
define("TIMEOUT", 20);

/**
 * Email
 */
define("FOUNTAIN_WISH_EMAIL", "fountain@wish.org");

/**
 * Misc
 */
define('BASE_URL', 'index.php');