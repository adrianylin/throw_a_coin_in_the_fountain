<?php

require_once "vendor/autoload.php";
require_once(dirname(__FILE__) . '/src/configs/Config.php');

/**
 * Serves as an entrypoint into the website. Displays a page according
 * to values in the controller and method request variables.
 */

$c = null; // name of controller
$m = null; // name of method

if (isset($_REQUEST['c']))
{
    $c = $_REQUEST['c'];
}
if (isset($_REQUEST['m']))
{
    $m = $_REQUEST['m'];
}

// display the landing page
if (($c == null && $m == null) ||
    ($c == 'LandingController' && $m == 'processRequest'))
{
    $landingController = new \lina\hw5\controllers\LandingController();
    $landingController->processRequest();
}
else if ($c == 'FountainWishController' && $m == 'processRequest')
{
    $fountainWishController = new \lina\hw5\controllers\FountainWishController();
    $fountainWishController->processRequest();
}
else
{
    echo "Page not found";
}
