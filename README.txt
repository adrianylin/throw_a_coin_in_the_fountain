To configure this project:

1) Get the dependencies (FPDF, Stripe, and Simpletest) by executing 'composer install'

2) Set variables in src/configs/Config.php

3) Initialize the database by executing 'php CreateDB.php' in src/configs/
